﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DseMarketDepthScraper.Helpers;
using DseMarketDepthScraper.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DseMarketDepthScraper.Workers
{
    public class MarketDepthScraper : BackgroundService
    {
        private readonly ILogger<MarketDepthScraper> _logger;
        private readonly IDepthDataService _depthDataService;

        public MarketDepthScraper(ILogger<MarketDepthScraper> logger, IDepthDataService depthDataService)
        {
            _logger = logger;
            _depthDataService = depthDataService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Worker starting at: {time}", DateTimeOffset.Now);
            stoppingToken.Register(() => _logger.LogInformation("Worker starting at: {time}", DateTimeOffset.Now));

            await Task.Delay(5000, stoppingToken);

            while (!stoppingToken.IsCancellationRequested)
            {
                await _depthDataService.AcquireCurrentDepthDataFromWeb(stoppingToken);
                await Task.Delay(ServiceConstants.IntervalMillisecond, stoppingToken);
            }
        }
    }
}