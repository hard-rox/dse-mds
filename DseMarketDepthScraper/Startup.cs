using System;
using DseMarketDepthScraper.Helpers;
using DseMarketDepthScraper.Hubs;
using DseMarketDepthScraper.Services;
using DseMarketDepthScraper.Workers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Polly;

namespace DseMarketDepthScraper
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        private const string WebAppCorsPolicy = "WebAppCorsPolicy";
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient("MarketDepthClient", opt =>
                {
                    opt.BaseAddress = new Uri(@"https://www.dse.com.bd/bshis_new1_nf.php");
                    opt.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                })
                .AddTransientHttpErrorPolicy(policy => policy.WaitAndRetryAsync(ServiceConstants.HttpCallRetryCount,
                    _ => TimeSpan.FromMilliseconds(ServiceConstants.HttpCallRetryDelay)));

            services.AddCors(options =>
            {
                //TODO: Origin will be added...
                options.AddPolicy(WebAppCorsPolicy, builder =>
                {
                    builder.WithOrigins("http://127.0.0.1:5500")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });

            services.AddSignalR();

            services.AddDbContext<MarketDataContext>();

            services.AddSingleton<IDepthDataService, DepthDataService>();

            services.AddHostedService<MarketDepthScraper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(WebAppCorsPolicy);

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/depth.json", async context =>
                {
                    var service = (IDepthDataService)app.ApplicationServices.GetService(typeof(IDepthDataService));
                    context.Response.Headers.Add("Accept-Encoding", "application/json");
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(
                         await service.Get(), new JsonSerializerSettings()
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }));
                });
                endpoints.MapGet("/depth/{instrumentId}.json", async context =>
                {
                    var service = (IDepthDataService)app.ApplicationServices.GetService(typeof(IDepthDataService));
                    var instrument = context.Request.RouteValues["instrumentId"].ToString();
                    context.Response.Headers.Add("Accept-Encoding", "application/json");
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(
                         await service.Get(instrument), new JsonSerializerSettings()
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }));
                });
                endpoints.MapHub<MarketDepthHub>("/depth-notification");
            });
        }
    }
}
