﻿using System.Text.RegularExpressions;

namespace DseMarketDepthScraper.Helpers
{
    internal static class Extensions
    {
        internal static decimal GetDecimalValue(this string inputString)
        {
            var decimalString = Regex.Match(inputString, @"\d+.+\d").Value;
            decimal.TryParse(decimalString, out var value);
            return value;
        }

        internal static int GetIntValue(this string inputString)
        {
            var decimalString = Regex.Match(inputString, @"\d+").Value;
            int.TryParse(decimalString, out var value);
            return value;
        }
    }
}
