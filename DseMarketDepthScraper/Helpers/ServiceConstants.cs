﻿namespace DseMarketDepthScraper.Helpers
{
    internal static class ServiceConstants
    {
        internal const int IntervalMillisecond = 500;
        internal const int HttpCallRetryCount = 3;
        internal const double HttpCallRetryDelay = 100;
    }
}
