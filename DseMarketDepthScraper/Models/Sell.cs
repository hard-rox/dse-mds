﻿namespace DseMarketDepthScraper.Models
{
    public class Sell
    {
        public decimal Price { get; set; }
        public int Volume { get; set; }
    }
}