﻿using System.Collections.Generic;

namespace DseMarketDepthScraper.Models
{
    public class InstrumentDepth
    {
        public string Instrument { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal LastTradePrice { get; set; }
        public decimal YesterdayClosingPrice { get; set; }
        public decimal ClosePrice { get; set; }
        public decimal DayHigh { get; set; }
        public decimal DayLow { get; set; }
        public int TradeCount { get; set; }
        public int TotalVolume { get; set; }
        public decimal TotalValueInMillion { get; set; }

        public ICollection<Buy> Buys { get; set; }
        public ICollection<Sell> Sells { get; set; }
    }
}