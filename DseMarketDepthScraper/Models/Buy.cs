﻿namespace DseMarketDepthScraper.Models
{
    public class Buy
    {
        public decimal Price { get; set; }
        public int Volume { get; set; }
    }
}