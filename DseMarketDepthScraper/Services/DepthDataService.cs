﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DseMarketDepthScraper.Helpers;
using DseMarketDepthScraper.Hubs;
using DseMarketDepthScraper.Models;
using DseMarketDepthScraper.Workers;
using HtmlAgilityPack;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DseMarketDepthScraper.Services
{
    internal class DepthDataService : IDepthDataService
    {
        private readonly ILogger<MarketDepthScraper> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IHubContext<MarketDepthHub> _hub;

        private List<InstrumentDepth> previousDepthData = new List<InstrumentDepth>();

        //private string TestFromFile(string instrument)
        //{
        //    const string path = @"D:\Projects\DseMarketDepthScraper\Sample.html";
        //    var file = File.ReadAllText(path);
        //    file = file.Replace("ADNTEL", instrument);
        //    return file;
        //}

        private async Task<InstrumentDepth> GetDepthData(string instrumentName, CancellationToken stoppingToken)
        {
            var bodyData = new Dictionary<string, string>
            {
                { "inst", instrumentName.Trim() }
            };
            var httpClient = _httpClientFactory.CreateClient("MarketDepthClient");
            var content = new FormUrlEncodedContent(bodyData);
            var response = await httpClient.PostAsync("", content, stoppingToken);
            if (response.IsSuccessStatusCode == false)
            {
                _logger.LogCritical("Failed with HTTP Status Code {statusCode} at: {time}", response.StatusCode, DateTimeOffset.Now);
            }

            var responseString = await response.Content.ReadAsStringAsync();
            return ParseDepthData(responseString);
        }

        private InstrumentDepth ParseDepthData(string htmlString)
        {
            try
            {
                //Console.WriteLine(htmlString);
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(htmlString);
                var instrument = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[3]//td//div//a//font").InnerHtml;
                var openPrice = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[2]//td[2]").InnerHtml;
                var daysHigh = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[2]//td[4]").InnerHtml;
                var ltp = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[3]//td[2]").InnerHtml;
                var daysLow = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[3]//td[4]").InnerHtml;
                var ycp = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[4]//td[2]").InnerHtml;
                var tradeCount = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[4]//td[4]").InnerHtml;
                var cp = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[5]//td[2]").InnerHtml;
                var totalVol = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[5]//td[4]").InnerHtml;
                var totalValMn = htmlDoc.DocumentNode
                    .SelectSingleNode("//table//tr[5]//td//table//tr[4]//td[2]//table//tr[6]//td[4]").InnerHtml;

                //Console.WriteLine(instrument + "\n" + openPrice + "\n" + daysHigh + "\n" + ltp + "\n" + daysLow + "\n" + ycp + "\n" + tradeCount + "\n" + cp + "\n" + totalVol + "\n" + totalValMn);

                var depth = new InstrumentDepth()
                {
                    Instrument = instrument.Trim(),
                    OpenPrice = openPrice.GetDecimalValue(),
                    DayHigh = daysHigh.GetDecimalValue(),
                    LastTradePrice = ltp.GetDecimalValue(),
                    DayLow = daysLow.GetDecimalValue(),
                    YesterdayClosingPrice = ycp.GetDecimalValue(),
                    TradeCount = tradeCount.GetIntValue(),
                    ClosePrice = cp.GetDecimalValue(),
                    TotalVolume = totalVol.GetIntValue(),
                    TotalValueInMillion = totalValMn.GetDecimalValue(),
                    Buys = new List<Buy>(),
                    Sells = new List<Sell>()
                };

                var buySellNodes = htmlDoc.DocumentNode
                    .SelectNodes("//table//tr[5]//td//table//tr//td[2]//table//tr//td//table");
                var buyNodes = buySellNodes[0].Elements("tr").Skip(2);
                var sellNodes = buySellNodes[1].Elements("tr").Skip(2);
                foreach (var buyNode in buyNodes)
                {
                    var price = buyNode.Elements("td").ToList()[0].Element("div").InnerHtml;
                    var volume = buyNode.Elements("td").ToList()[1].Element("div").InnerHtml;
                    depth.Buys.Add(new Buy() { Price = price.GetDecimalValue(), Volume = volume.GetIntValue() });
                }

                foreach (var sellNode in sellNodes)
                {
                    var price = sellNode.Elements("td").ToList()[0].Element("div").InnerHtml;
                    var volume = sellNode.Elements("td").ToList()[1].Element("div").InnerHtml;
                    depth.Sells.Add(new Sell() { Price = price.GetDecimalValue(), Volume = volume.GetIntValue() });
                }
                return depth;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void SaveDepthData(string instrument, InstrumentDepth depthData, CancellationToken stoppingToken)
        {
            var index = previousDepthData.FindIndex(x => x.Instrument == instrument);
            if (index != -1) previousDepthData[index] = depthData;
            else previousDepthData.Add(depthData);
        }

        private async Task Notify(InstrumentDepth depthData, CancellationToken stoppingToken)
        {
            Console.WriteLine(JsonConvert.SerializeObject(depthData));
            await _hub.Clients.All.SendAsync("depth-notification", depthData, stoppingToken);
        }

        public DepthDataService(ILogger<MarketDepthScraper> logger, IHttpClientFactory httpClientFactory, IHubContext<MarketDepthHub> hub)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _hub = hub;
        }

        public async Task AcquireCurrentDepthDataFromWeb(CancellationToken stoppingToken)
        {
            var instruments = Instruments.All;
            foreach (var instrument in instruments)
            {
                var depthData = await GetDepthData(instrument, stoppingToken);
                Console.WriteLine("Got data: " + instrument);

                //string path = @"data.txt";
                //// This text is added only once to the file.
                //if (!File.Exists(path))
                //{
                //    // Create a file to write to.
                //    await using (StreamWriter sw = File.CreateText(path)) ;
                //}

                //// This text is always added, making the file longer over time
                //// if it is not deleted.
                //await using (StreamWriter sw = File.AppendText(path))
                //{
                //    sw.WriteLine(JsonConvert.SerializeObject(depthData, Formatting.Indented));
                //}
                if (previousDepthData.Count(x => x.Instrument == instrument) > 0 &&
                    JsonConvert.SerializeObject(previousDepthData.FirstOrDefault(x => x.Instrument == instrument)) ==
                    JsonConvert.SerializeObject(depthData)) continue;
                await Notify(depthData, stoppingToken); 
                SaveDepthData(instrument, depthData, stoppingToken);
            }
        }

        public async Task<IEnumerable<InstrumentDepth>> Get()
        {
            return await Task.FromResult(previousDepthData);
        }

        public async Task<InstrumentDepth> Get(string instrument)
        {
            return await Task.FromResult(previousDepthData.FirstOrDefault(x => x.Instrument == instrument));
        }
    }
}