﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DseMarketDepthScraper.Models;

namespace DseMarketDepthScraper.Services
{
    public interface IDepthDataService
    {
        Task AcquireCurrentDepthDataFromWeb(CancellationToken stoppingToken);
        Task<IEnumerable<InstrumentDepth>> Get();
        Task<InstrumentDepth> Get(string instrument);
    }
}